import React, { Component } from "react";
import "./App.css";

// import { HelloUser } from "./components"
// import { Header } from "./components"
// import { Footer } from "./components"

import { HelloUser, Header, Footer, Clock } from "./components"


class App extends Component {
    render() {
        return (
            <div>
                <Clock date={new Date()} />
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
                        <a className="navbar-brand" href="#">jojomojoorologio</a>
                        <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li className="nav-item active">
                                <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Link</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link disabled" href="#">Disabled</a>
                            </li>
                        </ul>
                        <form className="form-inline my-2 my-lg-0">
                            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                            <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                    </div>
                </nav>

                
                <div className="container">
                    <div className="row">
                        <Header title="Jojo's Blog"/>        
                    </div>
                    <div>
                        <HelloUser name="Matteo" lastname="Dello Ioio" />
                    </div>
                </div>
                <Footer companyName="Ciccio Bello s.r.l." vatCode="12345678901" />
            </div>
        );
    }
}

export default App;