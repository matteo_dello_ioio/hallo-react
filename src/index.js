import React from "react";
import ReactDOM from "react-dom";
import App from "./App.js";
// import { HelloUser } from "./components/HelloUser/HelloUser.js";

import 'bootstrap/dist/css/bootstrap.min.css';

import 'jquery/dist/jquery.min.js';
import 'popper.js/dist/popper.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';


ReactDOM.render(
    <App />,
    // <HelloUser name="Donald" lastname="Duck" />,
    document.getElementById("root")
);