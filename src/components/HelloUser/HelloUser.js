import React from "react";
import ReactDOM from "react-dom";
import "./HelloUser.css";

export class HelloUser extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (<span>Hello, {this.props.name} {this.props.lastname}</span>);
    }
}

export default HelloUser;