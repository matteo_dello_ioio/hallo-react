import React from "react";
import ReactDOM from "react-dom";
import "./Header.css";

export class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (<header className="title">{this.props.title}</header>);
    }

    componentDidMount() {

    }
}

export default Header;