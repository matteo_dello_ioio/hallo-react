import React from "react";
import ReactDOM from "react-dom";
import "./Footer.css";

export class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <footer className="footer">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col"></div>
                        <div className="col">{this.props.companyName}</div>
                        <div className="col">P.I. {this.props.vatCode}</div>
                        <div className="col"></div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;