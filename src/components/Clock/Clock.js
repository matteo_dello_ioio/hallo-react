import React from "react";
import ReactDOM from "react-dom";
import "./Clock.css";

export class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }

    render() {
        return (<div>{this.state.date.toLocaleTimeString()}</div>);//<div></div>;
    }

    tick() {
        this.setState({
            date: new Date()
        });
    }

    componentDidMount() {
        this.timerID = setInterval(
          () => this.tick(),
          1000
        );
    }
    
    componentWillUnmount() {
        clearInterval(this.timerID);
    }
}

export default Clock;